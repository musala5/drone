package com.musala.drone.domain.drone.aggregates;

import com.musala.drone.application.commands.CheckBatteryLevelCommand;
import com.musala.drone.application.commands.CompleteLoadingCommand;
import com.musala.drone.application.commands.LoadDroneCommand;
import com.musala.drone.application.commands.RegisterDroneCommand;
import com.musala.drone.application.config.AppConfig;
import com.musala.drone.application.presentation.MedicalItemData;
import com.musala.drone.domain.drone.events.BatteryLevelCheckedEvent;
import com.musala.drone.domain.drone.events.DroneLoadedEvent;
import com.musala.drone.domain.drone.events.DroneLoadingInitiatedEvent;
import com.musala.drone.domain.drone.events.DroneRegisteredEvent;
import com.musala.drone.domain.drone.exceptions.DroneBatteryException;
import com.musala.drone.domain.drone.exceptions.DroneStateException;
import com.musala.drone.domain.drone.exceptions.DroneWeightLimitException;
import com.musala.drone.domain.drone.valueobjects.DroneModel;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Aggregate()
@NoArgsConstructor
@Slf4j
public class DroneAggregate {
    @AggregateIdentifier
    private String serialNumber;
    private DroneModel model;
    private int weightLimit;
    private float batteryLevel;
    private DroneState state;

    @CommandHandler
    public DroneAggregate(RegisterDroneCommand command) {
        DroneRegisteredEvent event = DroneRegisteredEvent.builder()
            .serialNumber(command.getSerialNumber())
            .batteryLevel(command.getBatteryLevel())
            .model(command.getModel())
            .state(DroneState.IDLE)
            .weightLimit(command.getWeightLimit())
            .build();
        apply(event);
    }

    @EventSourcingHandler
    public void on(DroneRegisteredEvent event) {
        this.serialNumber = event.getSerialNumber();
        this.model = event.getModel();
        this.weightLimit = event.getWeightLimit();
        this.batteryLevel = event.getBatteryLevel();
        this.state = event.getState();
    }

    @CommandHandler
    public void handle(LoadDroneCommand command, AppConfig appConfig) {
        checkIfDroneIdle();
        checkIfWeightLimitExceeded(command.getItems());
        checkIfBatteryLow(appConfig.getAllowedLowestBatteryLevel());

        DroneLoadingInitiatedEvent event = DroneLoadingInitiatedEvent.builder()
                .serialNumber(serialNumber)
                .items(command.getItems())
                .initiatedAt(LocalDateTime.now())
                .build();
        apply(event);
    }

    @EventSourcingHandler
    public void on(DroneLoadingInitiatedEvent event) {
        this.state = DroneState.LOADING;
    }

    @CommandHandler
    public void handle(CompleteLoadingCommand command) {
        DroneLoadedEvent event = DroneLoadedEvent.builder()
                .serialNumber(serialNumber)
                .items(command.getItems())
                .loadedAt(command.getLoadedAt())
                .build();
        apply(event);
    }

    @EventSourcingHandler
    public void on(DroneLoadedEvent event) {
        this.state = DroneState.LOADED;
    }

    @CommandHandler
    public void handle(CheckBatteryLevelCommand command) {
        BatteryLevelCheckedEvent event = BatteryLevelCheckedEvent.builder()
                .serialNumber(serialNumber)
                .batteryLevel(batteryLevel)
                .checkedAt(LocalDateTime.now())
                .build();
        apply(event);
    }

    private void checkIfBatteryLow(float allowedLowestBatterLevel) {
        if (this.batteryLevel < allowedLowestBatterLevel) {
            throw new DroneBatteryException("drone.battery-too-low");
        }
    }

    private void checkIfWeightLimitExceeded(List<@Valid MedicalItemData> items) {
        int totalWeight = items.stream()
                .reduce(0, (sum, item) -> sum + item.getWeight(), Integer::sum);
        if (totalWeight > this.weightLimit) {
            throw new DroneWeightLimitException();
        }
    }

    private void checkIfDroneIdle() {
        if (this.state != DroneState.IDLE) {
            throw new DroneStateException("drone.not-idle");
        }
    }
}
