package com.musala.drone.domain.drone.entities;

import com.musala.drone.domain.drone.valueobjects.DroneModel;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Document(collection = "drones")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Drone {
    @Id
    private String id;
    @Indexed
    private String serialNumber;
    private DroneModel model;
    private int weightLimit;
    private float batteryLevel;
    private DroneState state;
}
