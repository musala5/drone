package com.musala.drone.domain.drone.entities;

import com.musala.drone.application.presentation.MedicalItemData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Document(collection = "shipments")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Shipment {
    @Id
    private String id;
    @Indexed
    private String droneSerialNumber;
    private List<MedicalItemData> items;
    private LocalDateTime loadedAt;
}
