package com.musala.drone.domain.drone.exceptions;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneNotFoundException extends RuntimeException {
    public DroneNotFoundException() {
        super("drone.not-found");
    }
}
