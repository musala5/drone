package com.musala.drone.domain.drone.events;

import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Builder
@Data
public class BatteryLevelCheckedEvent {
    private String serialNumber;
    private float batteryLevel;
    private LocalDateTime checkedAt;
}
