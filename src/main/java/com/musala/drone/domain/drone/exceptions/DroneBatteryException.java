package com.musala.drone.domain.drone.exceptions;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneBatteryException extends RuntimeException {
    public DroneBatteryException(String message) {
        super(message);
    }
}
