package com.musala.drone.domain.drone.saga;

import com.musala.drone.application.commands.CompleteLoadingCommand;
import com.musala.drone.domain.drone.events.DroneLoadedEvent;
import com.musala.drone.domain.drone.events.DroneLoadingInitiatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Saga
@Slf4j(topic = "LoadDroneSaga")
public class LoadDroneSaga {
    private static final String ASSOCIATION_PROPERTY = "serialNumber";

    private transient CommandGateway commandGateway;

    @StartSaga
    @SagaEventHandler(associationProperty = ASSOCIATION_PROPERTY)
    public void on(DroneLoadingInitiatedEvent event) {
        // todo: time consuming task here
        CompleteLoadingCommand command = CompleteLoadingCommand.builder()
                .serialNumber(event.getSerialNumber())
                .items(event.getItems())
                .loadedAt(LocalDateTime.now())
                .build();
        commandGateway.send(command);
    }

    @EndSaga
    @SagaEventHandler(associationProperty = ASSOCIATION_PROPERTY)
    public void on(DroneLoadedEvent event) {
    }

    @Autowired
    public void setCommandGateway(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }
}
