package com.musala.drone.domain.drone.exceptions;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneWeightLimitException extends RuntimeException {
    public DroneWeightLimitException() {
        super("drone.weight-limit-exceeded");
    }
}
