package com.musala.drone.domain.drone.events;

import com.musala.drone.domain.drone.valueobjects.DroneModel;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Builder
@Data
public class DroneRegisteredEvent {
    private String serialNumber;
    private DroneModel model;
    private int weightLimit;
    private float batteryLevel;
    private DroneState state;
    private LocalDateTime registeredAt;
}
