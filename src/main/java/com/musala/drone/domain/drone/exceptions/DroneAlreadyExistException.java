package com.musala.drone.domain.drone.exceptions;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneAlreadyExistException extends RuntimeException {
    public DroneAlreadyExistException() {
        super("drone.already-exist");
    }
}
