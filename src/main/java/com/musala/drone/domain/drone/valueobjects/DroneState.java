package com.musala.drone.domain.drone.valueobjects;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
public enum DroneState {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}
