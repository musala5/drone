package com.musala.drone.domain.drone.events;

import com.musala.drone.application.presentation.MedicalItemData;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Builder
@Data
public class DroneLoadingInitiatedEvent {
    private String serialNumber;
    private List<MedicalItemData> items;
    private LocalDateTime initiatedAt;
}
