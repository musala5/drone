package com.musala.drone.domain.drone.exceptions;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneStateException extends RuntimeException {
    public DroneStateException(String message) {
        super(message);
    }
}
