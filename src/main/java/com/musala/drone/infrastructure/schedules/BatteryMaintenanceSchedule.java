package com.musala.drone.infrastructure.schedules;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */

import com.musala.drone.application.commands.CheckBatteryLevelCommand;
import com.musala.drone.domain.drone.entities.Drone;
import com.musala.drone.infrastructure.repositories.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

@Component
@EnableAsync
@RequiredArgsConstructor
@Slf4j(topic = "BatteryMaintenanceSchedule")
public class BatteryMaintenanceSchedule {
    private final CommandGateway commandGateway;
    private final DroneRepository droneRepository;

    private final AtomicLong recordSize = new AtomicLong(0);

    /**
     * Execute every 6 hours
     */
    @Async
    @Scheduled(cron = "0 */6 * * * *")
    public void batteryLevelMonitor() {
        log.info("Start battery level checks...");

        try(Stream<Drone> droneStream = droneRepository.streamDrones()) {
            droneStream.forEach(this::checkBatteryLevelOf);
        }

        log.info("{} drones - battery level checks completed", recordSize.get());
        recordSize.set(0);
    }

    private void checkBatteryLevelOf(Drone drone) {
        CheckBatteryLevelCommand command = CheckBatteryLevelCommand.builder()
                .serialNumber(drone.getSerialNumber())
                .build();
        commandGateway.send(command);
        recordSize.getAndIncrement();

    }
}
