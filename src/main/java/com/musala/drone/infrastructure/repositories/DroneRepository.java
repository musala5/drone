package com.musala.drone.infrastructure.repositories;

import com.musala.drone.domain.drone.entities.Drone;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public interface DroneRepository extends MongoRepository<Drone, String> {
    Optional<Drone> getDroneBySerialNumber(String serialNumber);
    Page<Drone> getDronesByState(DroneState state, Pageable page);
    @Query("{}, {serialNumber: 1, batteryLevel: 1}")
    Stream<Drone> streamDrones();
}
