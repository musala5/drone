package com.musala.drone.infrastructure.repositories;

import com.musala.drone.domain.drone.entities.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public interface ShipmentRepository extends MongoRepository<Shipment, String> {
    Page<Shipment> getShipmentsByDroneSerialNumber(String droneSerialNumber, Pageable page);
}
