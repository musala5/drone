package com.musala.drone.application.api;

import com.musala.drone.domain.drone.events.DroneLoadedEvent;
import com.musala.drone.domain.drone.events.DroneLoadingInitiatedEvent;
import com.musala.drone.domain.drone.events.DroneRegisteredEvent;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public interface DroneEventHandler {
    void on(DroneRegisteredEvent event);
    void on(DroneLoadingInitiatedEvent event);
    void on(DroneLoadedEvent event);
}
