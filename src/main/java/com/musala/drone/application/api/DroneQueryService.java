package com.musala.drone.application.api;

import com.musala.drone.application.presentation.BatteryLevel;
import com.musala.drone.application.presentation.DroneData;
import com.musala.drone.application.presentation.ShipmentData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public interface DroneQueryService {
    /**
     * Fetch loaded medication items for a given drone
     * @param serialNumber The drone serial number
     * @return The shipments
     */
    Page<ShipmentData> getDroneShipments(String serialNumber, Pageable page);

    /**
     * Fetch available drones for loading
     * @param page The page to fetch
     * @return The drones
     */
    Page<DroneData> getAvailableDrones(Pageable page);

    /**
     * Get drone battery level for a given drone
     * @param serialNumber The drone serial number
     * @return The battery level
     */
    BatteryLevel getDroneBatteryLevel(String serialNumber);
}
