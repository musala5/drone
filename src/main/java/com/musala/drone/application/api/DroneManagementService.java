package com.musala.drone.application.api;

import com.musala.drone.application.commands.LoadDroneCommand;
import com.musala.drone.application.commands.RegisterDroneCommand;
import com.musala.drone.application.presentation.DroneOperationResult;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
public interface DroneManagementService {
    DroneOperationResult registerDrone(RegisterDroneCommand command);
    DroneOperationResult loadDrone(LoadDroneCommand command);
}
