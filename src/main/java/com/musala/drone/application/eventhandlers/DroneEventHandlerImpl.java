package com.musala.drone.application.eventhandlers;

import com.musala.drone.application.api.DroneEventHandler;
import com.musala.drone.domain.drone.events.DroneLoadedEvent;
import com.musala.drone.domain.drone.events.DroneLoadingInitiatedEvent;
import com.musala.drone.domain.drone.events.DroneRegisteredEvent;
import com.musala.drone.domain.drone.entities.Drone;
import com.musala.drone.domain.drone.entities.Shipment;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import com.musala.drone.infrastructure.repositories.DroneRepository;
import com.musala.drone.infrastructure.repositories.ShipmentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Component
@RequiredArgsConstructor
@ProcessingGroup("drone-group")
@Slf4j
public class DroneEventHandlerImpl implements DroneEventHandler {
    private final DroneRepository droneRepository;
    private final ShipmentRepository shipmentRepository;

    @EventHandler
    @Override
    public void on(DroneRegisteredEvent event) {
        Drone drone = Drone.builder()
                .serialNumber(event.getSerialNumber())
                .model(event.getModel())
                .state(event.getState())
                .batteryLevel(event.getBatteryLevel())
                .weightLimit(event.getWeightLimit())
                .build();
        droneRepository.save(drone);
    }

    @EventHandler
    @Override
    public void on(DroneLoadingInitiatedEvent event) {
        droneRepository.getDroneBySerialNumber(event.getSerialNumber())
                .ifPresent(drone -> {
                    drone.setState(DroneState.LOADING);
                    droneRepository.save(drone);
                });
    }

    @EventHandler
    @Override
    public void on(DroneLoadedEvent event) {
        droneRepository.getDroneBySerialNumber(event.getSerialNumber())
            .ifPresent(drone -> {
                drone.setState(DroneState.LOADING);
                droneRepository.save(drone);
            });
        Shipment shipment = Shipment.builder()
                .droneSerialNumber(event.getSerialNumber())
                .items(event.getItems())
                .loadedAt(event.getLoadedAt())
                .build();
        shipmentRepository.save(shipment);
    }
}
