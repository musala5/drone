package com.musala.drone.application.exceptions;

import com.musala.drone.application.presentation.ReturnCode;
import lombok.Getter;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Getter
public class AppException extends RuntimeException {
    protected ReturnCode returnCode;
    protected String message;

    public AppException(String message) {
        this(ReturnCode.FAILED, message);
    }

    public AppException(ReturnCode errorCode, String message) {
        super(message);
        this.returnCode = errorCode;
        this.message = message;
    }
}
