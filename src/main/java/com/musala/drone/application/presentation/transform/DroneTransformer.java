package com.musala.drone.application.presentation.transform;

import com.musala.drone.application.presentation.DroneData;
import com.musala.drone.domain.drone.entities.Drone;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class DroneTransformer {
    public static DroneData transform(Drone drone) {
        return DroneData.builder()
                .id(drone.getId())
                .serialNumber(drone.getSerialNumber())
                .batteryLevel(drone.getBatteryLevel())
                .model(drone.getModel())
                .state(drone.getState())
                .weightLimit(drone.getWeightLimit())
                .build();
    }
}
