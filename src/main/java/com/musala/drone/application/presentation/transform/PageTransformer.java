package com.musala.drone.application.presentation.transform;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class PageTransformer {
    public static <E, R> Page<R> transform(Page<E> result, Function<E, R> mapper) {
        List<R> content = result.stream()
                .map(mapper)
                .collect(Collectors.toList());
        return new PageImpl(content, result.getPageable(), result.getTotalElements());
    }
}
