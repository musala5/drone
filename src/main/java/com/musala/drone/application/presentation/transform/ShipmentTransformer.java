package com.musala.drone.application.presentation.transform;

import com.musala.drone.application.presentation.ShipmentData;
import com.musala.drone.domain.drone.entities.Shipment;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
public class ShipmentTransformer {
    public static ShipmentData transform(Shipment shipment) {
        return ShipmentData.builder()
                .id(shipment.getId())
                .droneSerialNumber(shipment.getDroneSerialNumber())
                .items(shipment.getItems())
                .loadedAt(shipment.getLoadedAt())
                .build();
    }
}
