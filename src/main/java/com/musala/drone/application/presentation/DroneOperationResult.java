package com.musala.drone.application.presentation;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@RequiredArgsConstructor
@Builder
@Getter
public class DroneOperationResult {
    private final String serialNumber;
    private final String message;
}
