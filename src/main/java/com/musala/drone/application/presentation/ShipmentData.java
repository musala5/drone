package com.musala.drone.application.presentation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ShipmentData {
    private String id;
    private String droneSerialNumber;
    private List<MedicalItemData> items;
    private LocalDateTime loadedAt;
}
