package com.musala.drone.application.presentation;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
public enum ReturnCode {
    SUCCESS,
    FAILED,
    NOT_FOUND,
    ACCESS_DENIED;
}
