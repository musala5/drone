package com.musala.drone.application.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@AllArgsConstructor
@Data
public class BatteryLevel {
    private float value;
}
