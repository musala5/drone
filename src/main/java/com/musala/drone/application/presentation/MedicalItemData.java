package com.musala.drone.application.presentation;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Data
public class MedicalItemData {
    @Pattern(regexp = "^[a-zA-Z0-9_.-]*$")
    private String name;

    @Min(0)
    private int weight;

    @Pattern(regexp = "^[A-Z0-9_.-]*$")
    private String code;

    @NotBlank
    private String imageUrl;
}
