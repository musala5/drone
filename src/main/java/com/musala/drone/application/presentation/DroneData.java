package com.musala.drone.application.presentation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.musala.drone.domain.drone.valueobjects.DroneModel;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DroneData {
    private String id;
    private String serialNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private DroneModel model;
    private int weightLimit;
    private float batteryLevel;
    private DroneState state;
}
