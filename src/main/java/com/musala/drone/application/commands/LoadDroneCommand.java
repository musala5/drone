package com.musala.drone.application.commands;

import com.musala.drone.application.presentation.MedicalItemData;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Data
public class LoadDroneCommand {
    @TargetAggregateIdentifier
    @NotBlank
    private String serialNumber;
    private List<@Valid MedicalItemData> items;
}
