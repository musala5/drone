package com.musala.drone.application.commands.interceptors;

import com.musala.drone.application.commands.RegisterDroneCommand;
import com.musala.drone.domain.drone.exceptions.DroneAlreadyExistException;
import com.musala.drone.infrastructure.repositories.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Component
@RequiredArgsConstructor
@Slf4j(topic = "RegisterDroneCommandInterceptor")
public class RegisterDroneCommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    private final CommandBus commandBus;
    private final DroneRepository droneRepository;

    @PostConstruct
    public void init() {
        commandBus.registerDispatchInterceptor(this);
    }

    @Nonnull
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(@Nonnull List<? extends CommandMessage<?>> list) {
        return (index, command) -> {
            // Command interceptors are called for every command,
            // so ensure to check the command we're interested in.
            if (RegisterDroneCommand.class.getSimpleName().contains(command.getPayloadType().getSimpleName())) {
                RegisterDroneCommand registerDroneCommand = (RegisterDroneCommand) command.getPayload();
                droneRepository.getDroneBySerialNumber(registerDroneCommand.getSerialNumber())
                        .ifPresent(drone -> {
                            throw new DroneAlreadyExistException();
                        });
            }
            return command;
        };
    }
}
