package com.musala.drone.application.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Data
@Builder
public class CheckBatteryLevelCommand {
    @TargetAggregateIdentifier
    private String serialNumber;
}
