package com.musala.drone.application.commands;

import com.musala.drone.domain.drone.valueobjects.DroneModel;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Data
public class RegisterDroneCommand {
    @TargetAggregateIdentifier
    @Size(max = 100)
    private String serialNumber;

    private DroneModel model;

    private int weightLimit;

    @DecimalMin("0.0")
    @DecimalMax("1.0")
    private float batteryLevel;
}
