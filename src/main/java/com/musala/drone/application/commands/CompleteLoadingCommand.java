package com.musala.drone.application.commands;

import com.musala.drone.application.presentation.MedicalItemData;
import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Data
@Builder
public class CompleteLoadingCommand {
    @TargetAggregateIdentifier
    private String serialNumber;
    private List<MedicalItemData> items;
    private LocalDateTime loadedAt;
}
