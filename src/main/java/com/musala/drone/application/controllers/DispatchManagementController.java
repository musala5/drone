package com.musala.drone.application.controllers;

import com.musala.drone.application.annotations.ApiResponseWrap;
import com.musala.drone.application.annotations.CanLoadDrone;
import com.musala.drone.application.annotations.CanRegisterDrone;
import com.musala.drone.application.api.DroneManagementService;
import com.musala.drone.application.commands.LoadDroneCommand;
import com.musala.drone.application.commands.RegisterDroneCommand;
import com.musala.drone.application.presentation.DroneOperationResult;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@RestController
@RequestMapping("/dispatch/management")
@RequiredArgsConstructor
@ApiResponseWrap
public class DispatchManagementController {
    private final DroneManagementService droneManagementService;

    @CanRegisterDrone
    @PostMapping("register-drone")
    public DroneOperationResult registerDrone(@RequestBody @Validated RegisterDroneCommand command) {
        return droneManagementService.registerDrone(command);
    }

    @CanLoadDrone
    @PostMapping("load-drone")
    public DroneOperationResult loadDrone(@RequestBody @Validated LoadDroneCommand command) {
        return droneManagementService.loadDrone(command);
    }
}
