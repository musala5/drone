package com.musala.drone.application.controllers;

import com.musala.drone.application.annotations.*;
import com.musala.drone.application.api.DroneQueryService;
import com.musala.drone.application.presentation.BatteryLevel;
import com.musala.drone.application.presentation.DroneData;
import com.musala.drone.application.presentation.ShipmentData;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@RestController
@RequestMapping("/dispatch/query")
@RequiredArgsConstructor
@ApiResponseWrap
public class DispatchQueryController {
    private final DroneQueryService droneQueryService;

    @CanFetchShipments
    @GetMapping("drone/{serialNumber}/shipments")
    public Page<ShipmentData> fetchShipments(@PathVariable("serialNumber") String serialNumber, Pageable page) {
        return droneQueryService.getDroneShipments(serialNumber, page);
    }

    @CanFetchDrones
    @GetMapping("available/drones")
    public Page<DroneData> fetchAvailableDrones(Pageable page) {
        return droneQueryService.getAvailableDrones(page);
    }

    @CanFetchBatteryLevel
    @GetMapping("drone/{serialNumber}/battery-level")
    public BatteryLevel getDroneBatteryLevel(@PathVariable("serialNumber") String serialNumber) {
        return droneQueryService.getDroneBatteryLevel(serialNumber);
    }
}