package com.musala.drone.application.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j(topic = "SecurityConfig")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    @Override
    protected void configure(AuthenticationManagerBuilder builder) {
        appConfig.getApiUsers().forEach(apiUser -> {
            try {
                builder.inMemoryAuthentication()
                        .withUser(apiUser.getUsername())
                        .password(encoder.encode(apiUser.getPassword()))
                        .roles(apiUser.getRole());
            } catch (Exception e) {
                log.error("Error configuring users access", e);
            }
        });
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .authorizeRequests()
            .antMatchers("/**").authenticated()
            .anyRequest().permitAll()
            .and()
            .httpBasic()
            .and()
            .csrf()
            .disable();
    }
}
