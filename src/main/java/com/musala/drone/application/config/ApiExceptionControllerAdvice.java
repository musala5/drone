package com.musala.drone.application.config;

import com.musala.drone.application.exceptions.AppException;
import com.musala.drone.application.presentation.ApiResponse;
import com.musala.drone.application.presentation.FieldValidationError;
import com.musala.drone.application.presentation.ReturnCode;
import com.musala.drone.domain.drone.exceptions.DroneAlreadyExistException;
import com.musala.drone.domain.drone.exceptions.DroneNotFoundException;
import org.axonframework.modelling.command.AggregateNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@RestControllerAdvice
public class ApiExceptionControllerAdvice {

    @ExceptionHandler({AppException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiResponse handleBadRequestException(AppException ex) {
        return ApiResponse.builder()
                .code(ex.getReturnCode().name())
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler({DroneAlreadyExistException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiResponse handleDomainException(RuntimeException ex) {
        return ApiResponse.builder()
                .code(ReturnCode.FAILED.name())
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler(DroneNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiResponse handleException(DroneNotFoundException ex) {
        return ApiResponse.builder()
                .code(ReturnCode.NOT_FOUND.name())
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler(AggregateNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiResponse handleAggregateException(AggregateNotFoundException ex) {
        return ApiResponse.builder()
                .code(ReturnCode.NOT_FOUND.name())
                .message("drone.not-found")
                .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse handleDefaultException(Exception ex) {
        return ApiResponse.builder()
                .code(ReturnCode.FAILED.name())
                .message("Oops! something went wrong.")
                .build();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ApiResponse handleAccessDeniedException(AccessDeniedException ex) {
        return ApiResponse.builder()
                .code(ReturnCode.ACCESS_DENIED.name())
                .message("You do not have permission to execute action")
                .build();
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ApiResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        return ApiResponse
                .builder()
                .code(ReturnCode.FAILED.toString())
                .message("Invalid request body")
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {

        List<FieldValidationError> errorList = ex.getBindingResult().getFieldErrors().stream()
                .map(error -> FieldValidationError.builder()
                        .name(error.getField())
                        .message(error.getDefaultMessage())
                        .build()
                )
                .distinct()
                .collect(Collectors.toList());
        return ApiResponse
                .builder()
                .code(ReturnCode.FAILED.toString())
                .message("Request validation failed")
                .data(errorList)
                .build();
    }
}
