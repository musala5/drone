package com.musala.drone.application.config;

import com.musala.drone.application.annotations.ApiResponseWrap;
import com.musala.drone.application.presentation.ApiResponse;
import com.musala.drone.application.presentation.ReturnCode;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@ControllerAdvice(annotations = ApiResponseWrap.class)
public class ApiResponseWrapControllerAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        return ApiResponse.builder()
                .code(ReturnCode.SUCCESS.name())
                .message("successful")
                .data(o)
                .build();
    }
}
