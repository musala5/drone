package com.musala.drone.application.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Data
@Validated
@Component
@ConfigurationProperties("app")
public class AppConfig {
    private List<ApiUser> apiUsers;
    private float allowedLowestBatteryLevel;

    @Data
    public static class ApiUser {
        private String username;
        private String password;
        private String role;
    }
}
