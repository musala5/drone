package com.musala.drone.application.services;

import com.musala.drone.application.api.DroneQueryService;
import com.musala.drone.application.presentation.BatteryLevel;
import com.musala.drone.application.presentation.DroneData;
import com.musala.drone.application.presentation.ShipmentData;
import com.musala.drone.application.presentation.transform.DroneTransformer;
import com.musala.drone.application.presentation.transform.PageTransformer;
import com.musala.drone.application.presentation.transform.ShipmentTransformer;
import com.musala.drone.domain.drone.entities.Drone;
import com.musala.drone.domain.drone.entities.Shipment;
import com.musala.drone.domain.drone.exceptions.DroneNotFoundException;
import com.musala.drone.domain.drone.valueobjects.DroneState;
import com.musala.drone.infrastructure.repositories.DroneRepository;
import com.musala.drone.infrastructure.repositories.ShipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created 08/12/2022
 *
 * @author bilyamin
 */
@Service
@RequiredArgsConstructor
public class DroneQueryServiceImpl implements DroneQueryService {
    private final DroneRepository droneRepository;
    private final ShipmentRepository shipmentRepository;

    @Override
    public Page<ShipmentData> getDroneShipments(String serialNumber, Pageable page) {
        Page<Shipment> result = shipmentRepository.getShipmentsByDroneSerialNumber(serialNumber, page);
        return PageTransformer.transform(result, ShipmentTransformer::transform);
    }

    @Override
    public Page<DroneData> getAvailableDrones(Pageable page) {
        Page<Drone> result = droneRepository.getDronesByState(DroneState.IDLE, page);
        return PageTransformer.transform(result, DroneTransformer::transform);
    }

    @Override
    public BatteryLevel getDroneBatteryLevel(String serialNumber) {
        Drone drone = droneRepository.getDroneBySerialNumber(serialNumber)
                .orElseThrow(DroneNotFoundException::new);
        return new BatteryLevel(drone.getBatteryLevel());
    }
}
