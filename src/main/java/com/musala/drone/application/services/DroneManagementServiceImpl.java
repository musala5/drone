package com.musala.drone.application.services;

import com.musala.drone.application.api.DroneManagementService;
import com.musala.drone.application.commands.LoadDroneCommand;
import com.musala.drone.application.commands.RegisterDroneCommand;
import com.musala.drone.application.exceptions.AppException;
import com.musala.drone.application.presentation.DroneOperationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

/**
 * Created 07/12/2022
 *
 * @author bilyamin
 */
@Service
@RequiredArgsConstructor
@Slf4j(topic = "DroneManagementService")
public class DroneManagementServiceImpl implements DroneManagementService {
    private final CommandGateway commandGateway;

    @Override
    public DroneOperationResult registerDrone(RegisterDroneCommand command) {
        try {
            commandGateway.sendAndWait(command);
            return DroneOperationResult.builder()
                    .serialNumber(command.getSerialNumber())
                    .message("drone.registered-successful")
                    .build();
        } catch (CommandExecutionException e) {
            throw new AppException(e.getMessage());
        }
    }

    @Override
    public DroneOperationResult loadDrone(LoadDroneCommand command) {
        try {
            commandGateway.sendAndWait(command);
            return DroneOperationResult.builder()
                    .serialNumber(command.getSerialNumber())
                    .message("drone.loading-items")
                    .build();
        } catch (CommandExecutionException e) {
            throw new AppException(e.getMessage());
        }
    }
}
