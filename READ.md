## Drone Delivery Service

### Prerequisites

---
- Java 11
- Mongo DB V4.4.5

### Setup
- Clone Repository
  ```git clone https://gitlab.com/musala5/drone.git```
  

- Install Pom Dependencies
  
  ```cd project-folder```
  
  ``mvn clean install``
  

- Add Environment Variables
  
  ```ADMIN_USERNAME: ANY_USERNAME```
  
  ```ADMIN_PASSWORD: ANY_PASSWORD```
  
  ```AXONSERVER_TOKEN: MktkkzQDkBX97rOWWa79```


- Run Application

  ```mvn spring-boot:run```


- API Documentation/Swagger URL
```http://localhost:8080/swagger-ui/index.html```